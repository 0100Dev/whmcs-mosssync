<?php

require_once 'Actions.php';

if (!defined('WHMCS')) {
    die('This file cannot be accessed directly');
}

add_hook('DailyCronJob', 1, function () {
    (new DevApp\WHMCS\MOSSync\Actions())->sync();
});
