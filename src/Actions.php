<?php

namespace DevApp\WHMCS\MOSSync;

use WHMCS\Database\Capsule;

class Actions
{

    const MOSS_SYNC_PREFIX = 'MOSS Sync: ';

    /**
     * Sync the local DB
     *
     * @return void
     */
    public function sync()
    {
        $newRules = $this->_getRates();

        if ($newRules === null) {
            logActivity(self::MOSS_SYNC_PREFIX . 'Sync failed, as result from resource was null or false.');
            return;
        }

        $currentRules = Capsule::table('tbltax')->get();

        // Sync new rules
        foreach ($newRules as $country => $newRule) {
            if (isset($newRule->iso_duplicate)) {
                continue;
            }

            $foundRule = null;

            foreach ($currentRules as $key => $currentRule) {
                if ($currentRule->country === $country) {
                    $foundRule = $currentRule;
                    unset($currentRules[$key]); // Unset current rules to prevent it from deleting later
                }
            }

            if ($foundRule === null) {
                $this->_insertRule($country, $newRule);
            } else {
                $this->_updateRule($country, $foundRule, $newRule);
            }
        }

        // Remove left over TAX rules
        foreach ($currentRules as $currentRule) {
            $this->_deleteRule($currentRule);
        }

        logActivity(self::MOSS_SYNC_PREFIX . 'Successfully updated TAX rules.');
    }

    /**
     * Insert a rule in the DB
     *
     * @param $country
     * @param $rule
     *
     * @return void
     */
    private function _insertRule($country, $rule)
    {
        Capsule::table('tbltax')
            ->insert($this->_getRule($country, $rule));

        logActivity(self::MOSS_SYNC_PREFIX . 'Added TAX rule for country ' . $country . ' (' . $rule->standard_rate . ')');
    }

    /**
     * Update a rule in the DB
     *
     * @param $country
     * @param $fromRule
     * @param $newRule
     *
     * @return void
     */
    private function _updateRule($country, $fromRule, $newRule)
    {
        if ((float) $fromRule->taxrate === (float) $newRule->standard_rate) {
            return;
        }

        // Update TAX rule in WHMCS
        Capsule::table('tbltax')
            ->where('id', $fromRule->id)
            ->update($this->_getRule($country, $newRule));

        logActivity(self::MOSS_SYNC_PREFIX . 'Updated TAX rule for country ' . $country . ' (' . $fromRule->taxrate . ' => ' . $newRule->standard_rate . ')');
    }

    /**
     * Delete a rule from the DB
     *
     * @param $currentRule
     *
     * @return void
     */
    private function _deleteRule($currentRule)
    {
        Capsule::table('tbltax')
            ->where('id', $currentRule->id)
            ->delete();

        logActivity(self::MOSS_SYNC_PREFIX . 'Removed TAX rule for country ' . $currentRule->country . ' (' . $currentRule->taxrate . ')');
    }

    /**
     * Get the DB rule array for insert/updateing
     *
     * @param $country
     * @param $rule
     *
     * @return array
     */
    private function _getRule($country, $rule)
    {
        return [
            'level' => 1,
            'name' => 'VAT ' . $country,
            'state' => '',
            'country' => $country,
            'taxrate' => (float) $rule->standard_rate
        ];
    }

    /**
     * Get the rules from the document
     *
     * @return null|stdClass[]
     */
    private function _getRates()
    {
        curl_init();

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://wceuvatcompliance.s3.amazonaws.com/rates.json');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = json_decode(curl_exec($ch));
        curl_close($ch);

        if ($result === false || $result === null) {
            return null;
        }

        return $result->rates;
    }
}
