<?php

require_once 'Actions.php';

if (!defined('WHMCS')) {
    die('This file cannot be accessed directly');
}

function mosssync_config()
{
    return [
        'name' => 'MOSS Sync',
        'description' => 'Daily syncs all the VAT rules',
        'version' => '1.0',
        'author' => '<a href="http://devapp.nl/">Dev App</a>',
        'language' => 'dutch',
        'fields' => [
        ]
    ];
}

function mosssync_output()
{
    if (isset($_GET['sync'])) {
        (new DevApp\WHMCS\MOSSync\Actions())->sync();

        header('Location: systemactivitylog.php');
        return;
    }

    echo '<a href="?module=mosssync&sync">Manual sync</a>';
}
