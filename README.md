# WHMCS-MOSS Sync
Hooks into the daily cron and syncs the VAT rules to be correct for MOSS.

It will delete the VAT rules that are not found in the source document. It will update existing ones and add if no matching rule is found.

Uses [this](https://wceuvatcompliance.s3.amazonaws.com/rates.json) document to sync.

How to install
==============
1. Connect to FTP
2. Go to your `whmcs/modules/addons` directory.
3. Upload the contents of the directory named `upload` in this ZIP file.
6. Go to your admin panel => Setup => Addon Modules.
7. Activate the E-Boekhouden Import addon.
6. Configure the addon.

How to upgrade
==============
1. Connect to FTP
2. Go to your `whmcs/modules/addons` directory.
3. Upload the contents of the directory named `upload` in this ZIP file. (Overwrite old files)
